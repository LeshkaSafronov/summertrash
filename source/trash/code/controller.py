import os
import json

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

from threading import Thread
from Queue import Queue

from core.trash import Trash as CoreTrash
from core.counter import Counter
from trashes.models import Trash, Job
from django.forms import model_to_dict
from django.core.files.base import ContentFile
from core.singleton import singleton


class Worker(Thread):
    def __init__(self, queue, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self._queue = queue

    def run(self):
        logger.warning('Thread {} started'.format(self.name))
        while True:
            target, args, kwargs = self._queue.get()

            logger.debug('Thread {}: start target {}'.format(self.name, target.__name__))
            target(*args, **kwargs)
            logger.debug('Thread {}: finish target {}'.format(self.name, target.__name__))

            self._queue.task_done()


@singleton
class Controller(object):

    def __init__(self):
        self.trashes = {}
        self.get_db_trashes()
        self._queue = Queue()

        for _ in range(10):
            worker = Worker(self._queue)
            worker.start()

    def get_db_trashes(self):
        for trash in Trash.objects.all():
            self.init_trash(trash.id)

    def get_files(self, trash_id):
        trash = self.trashes[trash_id]
        return trash.get_files()

    def init_trash(self, trash_id):
        trash = Trash.objects.get(id=trash_id)

        params = model_to_dict(trash)
        params.pop('id')

        self.trashes[trash_id] = CoreTrash(**params)

    def add_files(self, trash_id, data):
        self._queue.put((self.start_operation, [trash_id, 'remove'], data))

    def edit_trash(self, trash_id):
        db_trash = Trash.objects.get(pk=trash_id)
        trash = self.trashes[trash_id]
        trash.update_settings(**model_to_dict(db_trash))

    def restore_files(self, trash_id, paths):
        self._queue.put((self.start_operation, [], {'trash_id': trash_id,
                                                    'method': 'restore',
                                                    'paths': paths,
                                                    'verbose': True,
                                                    'recursive': True,
                                                    'force': True,
                                                    'workers': 10,
                                                    'errors': 'ignore'}))

    def delete_files(self, trash_id, paths):
        self._queue.put((self.start_operation, [], {'trash_id': trash_id,
                                                    'method': 'erase',
                                                    'paths': paths,
                                                    'recursive': True,
                                                    'force': True,
                                                    'verbose': True,
                                                    'workers': 10,
                                                    'errors': 'ignore'}))

    def clean_trash(self, trash_id):
        self._queue.put((self.start_operation, [], {'trash_id': trash_id,
                                                    'method': 'clean',
                                                    'paths': [],
                                                    'recursive': True,
                                                    'force': True,
                                                    'verbose': True,
                                                    'workers': 10,
                                                    'errors': 'ignore'}))

    def start_operation(self, trash_id, method, **params):
        trash = self.trashes[trash_id]
        trash.autoclean()
        if method == 'remove':
            func = trash.remove
        elif method == 'restore':
            func = trash.restore
        elif method == 'clean':
            func = trash.clean
        else:
            func = trash.erase

        if method != 'destroy':
            job = Job.objects.create(name=method, trash_id=trash_id)
            params['callback'] = Counter(trash, job, params['paths'])

            outputs = func(**params)

            file_output = []
            for output in outputs:
                result = output['result']
                inner_file_output = {}
                for key, value in result.items():
                    inner_file_output[key] = CoreTrash.STATUS[value]
                file_output.append(inner_file_output)

            job.done = True
            job.result = ContentFile(json.dumps(file_output, indent=4).decode('ascii'))
            job.result.name = "job_result_{}.json".format(job.id)

            job.save()
        else:
            func(**params)
            self.trashes.pop(trash_id, None)



