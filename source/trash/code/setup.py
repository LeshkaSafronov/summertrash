from setuptools import setup, find_packages

setup(
    name='trash',
    version='1.0',
    packages=find_packages(),
    entry_points={
        'console_scripts':
            ['trash = core.main:main'],
    },
    install_requires=['prettytable'],
)