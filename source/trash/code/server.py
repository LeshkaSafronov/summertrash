import re
import json

import web.settings
web.settings.setup()

from urlparse import parse_qsl, parse_qs
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from controller import Controller
from trashes.models import Trash


HOST = '0.0.0.0'
PORT = 7000

class APIHandler(BaseHTTPRequestHandler):

    def __init__(self, request, client_address, server):
        self.ctx = Controller()
        BaseHTTPRequestHandler.__init__(self, request, client_address, server)

    def read_data(self):
        data_string = self.rfile.read(int(self.headers['Content-Length']))
        return json.loads(data_string)

    def _check_trash_exists(self, trash_id):
        try:
            Trash.objects.get(id=trash_id)
        except Trash.DoesNotExist:
            return False
        return True

    def do_POST(self):
        init_match = re.match(r'/trashes/(\d+)/init$', self.path)
        add_match = re.match(r'/trashes/(\d+)/add$', self.path)
        restore_match = re.match(r'/trashes/(\d+)/restore$', self.path)
        delete_match = re.match(r'/trashes/(\d+)/delete$', self.path)
        clean_match = re.match(r'/trashes/(\d+)/clean', self.path)
        edit_match = re.match(r'/trashes/(\d+)/edit', self.path)

        if init_match:
            trash_id = int(init_match.group(1))
            if not self._check_trash_exists(trash_id):
                self.send_response(404)
            else:
                self.send_response(200)
                self.ctx.init_trash(trash_id)

        if edit_match:
            trash_id = int(edit_match.group(1))
            if not self._check_trash_exists(trash_id):
                self.send_response(404)
            else:
                self.send_response(200)
                self.ctx.edit_trash(trash_id)

        elif add_match:
            trash_id = int(add_match.group(1))
            if not self._check_trash_exists(trash_id):
                self.send_response(404)
            else:
                self.send_response(200)
                data = self.read_data()
                self.ctx.add_files(trash_id, data)

        elif restore_match:
            trash_id = int(restore_match.group(1))
            if not self._check_trash_exists(trash_id):
                self.send_response(404)
            else:
                self.send_response(200)
                data = self.read_data()
                self.ctx.restore_files(trash_id, data['paths'])

        elif delete_match:
            trash_id = int(delete_match.group(1))
            if not self._check_trash_exists(trash_id):
                self.send_response(404)
            else:
                self.send_response(200)
                data = self.read_data()
                self.ctx.delete_files(trash_id, data['paths'])

        elif clean_match:
            trash_id = int(clean_match.group(1))
            if not self._check_trash_exists(trash_id):
                self.send_response(404)
            else:
                self.send_response(200)
                self.ctx.clean_trash(trash_id)
        else:
            self.send_response(404)

    def do_GET(self):
        get_match = re.match('/trashes/(\d+)$', self.path)
        if get_match:
            self.send_response(200)

            self.send_header('Content-Type', 'application/json')
            self.end_headers()

            trash_id = int(get_match.group(1))
            data = self.ctx.get_files(trash_id)
            self.wfile.write(json.dumps(data))


if __name__ == '__main__':
    controller = Controller()
    server = HTTPServer((HOST, PORT), APIHandler)
    server.serve_forever()