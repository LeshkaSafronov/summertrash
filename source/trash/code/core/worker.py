import logging
from threading import Thread


class Worker(Thread):

    def __init__(self, func, queue, lock, result):
        super(self.__class__, self).__init__()
        self._queue = queue
        self._func = func
        self._result = result
        self._lock = lock

    def run(self):
        while True:
            args, kwargs = self._queue.get()
            logging.warning('in Worker args --> {}'.format(args))
            logging.warning('in Worker kwargs --> {}'.format(kwargs))
            output = self._func(*args, **kwargs)
            with self._lock:
                if isinstance(self._result, dict):
                    self._result.update(output)
                elif isinstance(self._result, list):
                    self._result.append(output)
            self._queue.task_done()
