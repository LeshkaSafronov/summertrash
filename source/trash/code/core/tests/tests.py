import sys
sys.path.insert(0, '..')

import os
import unittest
import shutil
import contextlib
import exception

from config import Config
from trash import Trash

@contextlib.contextmanager
def lcd(path):
    old_path = os.getcwd()
    os.chdir(path)
    yield
    os.chdir(old_path)

class TrashTest(unittest.TestCase):

    POLYGON_DIR = os.path.expanduser('~/.polygon')
    TRASH_DIR = os.path.expanduser('~/.test_trash')

    def _get_paths(self, path):
        result = [path]
        for root, dirs, files in os.walk(path, topdown=False):
            result += [os.path.join(root, dir) for dir in dirs]
            result += [os.path.join(root, file) for file in files]
        return result

    def _get_size(self, path):
        result = os.path.getsize(path)
        for root, dirs, files in os.walk(path, topdown=False):
            dir_sizes = map(lambda path: os.path.getsize(os.path.join(root, path)), dirs)
            file_sizes = map(lambda path: os.path.getsize(os.path.join(root, path)), files)
            result += sum(dir_sizes)
            result += sum(file_sizes)
        return result

    def _get_paths_with_none_fail_reason(self, path):
        result = {path: None}
        for root, dirs, files in os.walk(path, topdown=False):
            result.update({os.path.join(root, dir): None for dir in dirs})
            result.update({os.path.join(root, file): None for file in files})
        return result

    def assertEqualDict(self, first_dict, second_dict):
        self.assertEqual(len(first_dict), len(second_dict))
        for path in first_dict.keys():
            self.assertIn(path, second_dict)
            self.assertEqual(first_dict[path], second_dict[path])

    def _gen_dir_with_files(self):
        self.dir_with_files_path = os.path.join(self.POLYGON_DIR, 'dir_with_files')
        os.mkdir(self.dir_with_files_path)

        with lcd(self.dir_with_files_path):
            for i in range(5):
                open('file_{}'.format(i), 'wb').close()
            os.mkdir('inner_dir')

            with lcd('inner_dir'):
                for i in range(5):
                    open('file_{}'.format(i), 'wb').close()

    def _gen_empty_dir(self):
        self.empty_dir_path = os.path.join(self.POLYGON_DIR, 'inner_empty_dir')
        os.mkdir(self.empty_dir_path)

    def _gen_empty_file(self):
        self.empty_filepath = os.path.join(self.POLYGON_DIR, 'empty_file')
        open(self.empty_filepath, 'w').close()

    def _gen_file(self):
        self.filepath = os.path.join(self.POLYGON_DIR, 'file')
        with open(self.filepath, 'w') as f:
            f.write('Test file')

    def _gen_protected_file(self):
        self.protected_file_path = os.path.join(self.POLYGON_DIR, 'protected_file')
        open(self.protected_file_path, 'w').close()
        os.chmod(self.protected_file_path, 0444)

    def _check_file_exist_in_trash(self, path):
        basename = os.path.basename(path)
        trash_path_names = map(lambda path: '_'.join(path.split('_')[:-1]), os.listdir(self.trash.location_path))
        return basename in trash_path_names

    def _get_filepath_without_hashname(self, trash_path):
        return '_'.join(trash_path.split('_')[:-1])

    def _check_consistency_of_result(self, trash_result):
        for file_result in trash_result:
            cfg_path = file_result['cfg_path']
            config = Config(cfg_path)
            config.read()

            result = file_result['result']

            trash_path = config['trash_path']
            real_trash_paths = self._get_paths(trash_path)

            deleted_path_basenames = [os.path.basename(path) for path, fail_reason in result.items() if not fail_reason]

            self.assertEqual(len(deleted_path_basenames), len(real_trash_paths))
            self.assertEqual(int(config['size']), self._get_size(trash_path))

            for path in real_trash_paths:
                if os.path.basename(path) == config['trash_name']:
                    filepath = self._get_filepath_without_hashname(path)
                else:
                    filepath = path

                filepath_basename = os.path.basename(filepath)
                self.assertIn(filepath_basename, deleted_path_basenames)

    def setUp(self):
        if os.path.exists(self.POLYGON_DIR):
            shutil.rmtree(self.POLYGON_DIR)

        if os.path.exists(self.TRASH_DIR):
            shutil.rmtree(self.TRASH_DIR)

        os.mkdir(self.POLYGON_DIR)

        self.trash = Trash(name='Trash-Test', location_path=self.TRASH_DIR)
        self.trash.logger.disabled = True

        self._gen_empty_file()
        self._gen_file()
        self._gen_dir_with_files()
        self._gen_empty_dir()
        self._gen_protected_file()

    def tearDown(self):
        if os.path.exists(self.POLYGON_DIR):
            shutil.rmtree(self.POLYGON_DIR)

        if os.path.exists(self.trash.location_path):
            shutil.rmtree(self.trash.location_path)

    def test_init_trash_directory(self):
        self.assertTrue(os.path.exists(self.trash.location_path))

    def test_delete_not_existed_file(self):
        not_existed_path = os.path.join(self.POLYGON_DIR, 'not_existed_file_or_directory')
        with self.assertRaises(exception.NotExistedFileOrDirectory):
            self.trash.remove([not_existed_path])
        self.assertFalse(self._check_file_exist_in_trash(not_existed_path))

    def test_delete_not_existed_file_force(self):
        not_existed_path = os.path.join(self.POLYGON_DIR, 'not_existed_file_or_directory')
        try:
            self.trash.remove([not_existed_path], force=True)
            self.assertFalse(self._check_file_exist_in_trash(not_existed_path))
        except exception.NotExistedFileOrDirectory:
            self.fail('Should not raise NotExistedFileOrDirectory')

    def test_force_delete_not_existed_threaded(self):
        try:
            not_existed_path = os.path.join(self.POLYGON_DIR, 'not_existed_file_or_directory')
            test_result = self._get_paths_with_none_fail_reason(not_existed_path)
            test_result[not_existed_path] = 'not_existed'

            trash_result = self.trash.remove([not_existed_path], force=True, workers=64, errors='ignore')

            result = trash_result[0]['result']
            self.assertEqualDict(test_result, result)
            self.assertFalse(self._check_file_exist_in_trash(not_existed_path))
        except exception.NotExistedFileOrDirectory:
            self.fail('Should not raise NotExistedFileOrDirectory')

    def test_delete_existed_file(self):
        test_result = {self.filepath: None}
        result = self.trash.remove([self.filepath])

        self.assertEqualDict(test_result, result[0]['result'])
        self.assertTrue(self._check_file_exist_in_trash(self.filepath))
        self._check_consistency_of_result(result)

    def test_delete_empty_dir(self):
        with self.assertRaises(exception.IsDirectoryException):
            self.trash.remove([self.empty_dir_path])
        self.assertFalse(self._check_file_exist_in_trash(self.empty_dir_path))

    def test_delete_empty_dir_threaded(self):
        try:
            test_result = self._get_paths_with_none_fail_reason(self.empty_dir_path)
            test_result[self.empty_dir_path] = 'is_directory'

            result = self.trash.remove([self.empty_dir_path], errors='ignore')
            self.assertEqualDict(test_result, result[0]['result'])
            self.assertFalse(self._check_file_exist_in_trash(self.empty_dir_path))
        except exception.IsDirectoryException:
            self.fail('Should not raise IsDirectoryException')

    def test_delete_empty_dir_force(self):
        with self.assertRaises(exception.IsDirectoryException):
            self.trash.remove([self.empty_dir_path], force=True)
        self.assertFalse(self._check_file_exist_in_trash(self.empty_dir_path))

    def test_delete_empty_dir_recursive(self):
        try:
            result = self.trash.remove([self.empty_dir_path], recursive=True)
            self._check_consistency_of_result(result)
            test_result = self._get_paths_with_none_fail_reason(self.empty_dir_path)
            self.assertEqualDict(test_result, result[0]['result'])
        except exception.IsDirectoryException:
            self.fail('Should not raise IsDirectoryException')
        self.assertTrue(self._check_file_exist_in_trash(self.empty_dir_path))

    def test_delete_dir_with_files(self):
        with self.assertRaises(exception.IsDirectoryException):
            self.trash.remove([self.dir_with_files_path])
        self.assertFalse(self._check_file_exist_in_trash(self.dir_with_files_path))

    def test_delete_dir_with_files_threaded(self):
        try:
            test_result = {self.dir_with_files_path: 'is_directory'}
            result = self.trash.remove([self.dir_with_files_path], workers=64, errors='ignore')

            self.assertEqualDict(result[0]['result'], test_result)
            self.assertFalse(self._check_file_exist_in_trash(self.dir_with_files_path))
        except exception.NotEmptyDirectoryException:
            self.fail('Should not raise NotEmptyDirectoryException')

    def test_delete_dir_with_files_flag_dir(self):
        with self.assertRaises(exception.NotEmptyDirectoryException):
            self.trash.remove([self.dir_with_files_path], dir=True)
        self.assertFalse(self._check_file_exist_in_trash(self.dir_with_files_path))

    def test_delete_dir_with_files_flag_recursive(self):
        try:
            test_result = self._get_paths_with_none_fail_reason(self.dir_with_files_path)
            result = self.trash.remove([self.dir_with_files_path], recursive=True)

            self.assertEqualDict(result[0]['result'], test_result)
            self.assertTrue(self._check_file_exist_in_trash(self.dir_with_files_path))
            self._check_consistency_of_result(result)

        except exception.NotEmptyDirectoryException:
            self.fail('Should not raise NotEmptyDirectoryException')

    def test_delete_dir_with_files_flag_recursive_threaded(self):
        try:
            test_result = self._get_paths_with_none_fail_reason(self.dir_with_files_path)
            result = self.trash.remove([self.dir_with_files_path], recursive=True, workers=64, errors='ignore')

            self.assertEqualDict(result[0]['result'], test_result)
            self.assertTrue(self._check_file_exist_in_trash(self.dir_with_files_path))
            self._check_consistency_of_result(result)

        except exception.NotEmptyDirectoryException:
            self.fail('Should not raise NotEmptyDirectoryException')

    def test_delete_write_protected_file(self):
        with self.assertRaises(exception.WriteProtectedFileOrDirectiry):
            self.trash.remove([self.protected_file_path])
        self.assertFalse(self._check_file_exist_in_trash(self.protected_file_path))

    def test_delete_write_protected_file_threaded(self):
        try:
            test_result = {self.protected_file_path: 'write_protected'}
            result = self.trash.remove([self.protected_file_path], workers=64, errors='ignore')
            self.assertEqualDict(result[0]['result'], test_result)
            self.assertFalse(self._check_file_exist_in_trash(self.protected_file_path))
        except exception.WriteProtectedFileOrDirectiry:
            self.fail('Should not raise WriteProtectedFileOrDirectiry')

    def test_delete_write_protected_file_force(self):
        try:
            test_result = {self.protected_file_path: None}
            result = self.trash.remove([self.protected_file_path], force=True)

            self.assertEqualDict(result[0]['result'], test_result)
            self.assertTrue(self._check_file_exist_in_trash(self.protected_file_path))
            self._check_consistency_of_result(result)
        except exception.WriteProtectedFileOrDirectiry:
            self.fail('Should not raise WriteProtectedFileOrDirectiry')

    def test_delete_permission_denied_dir(self):
        os.chmod(self.dir_with_files_path, 0111)
        with self.assertRaises(exception.PermissionDenied):
            self.trash.remove([self.dir_with_files_path], recursive=True, force=True)
        os.chmod(self.dir_with_files_path, 0777)
        self.assertFalse(self._check_file_exist_in_trash(self.dir_with_files_path))

    def test_delete_permission_denied_dir_threaded(self):
        os.chmod(self.dir_with_files_path, 0111)
        try:
            test_result = {self.dir_with_files_path: 'permission_denied'}
            result = self.trash.remove([self.dir_with_files_path],
                                                     recursive=True,
                                                     force=True,
                                                     workers=64,
                                                     errors='ignore')
            self.assertEqualDict(test_result, result[0]['result'])
        except exception.PermissionDenied:
            self.fail('Should not raise PermissionDenied')

        os.chmod(self.dir_with_files_path, 0777)
        self.assertFalse(self._check_file_exist_in_trash(self.dir_with_files_path))

    def test_hashname_for_uniq(self):
        self.trash.remove([self.dir_with_files_path], recursive=True, force=True)
        self._gen_dir_with_files()
        self.trash.remove([self.dir_with_files_path], recursive=True, force=True)

        hashnames = [path.split('_')[-1] for path in os.listdir(self.trash.location_path) if not path.endswith('.cfg')]
        self.assertEqual(len(set(hashnames)), len(hashnames))

    def test_get_config_filepath(self):
        trash_result = self.trash.remove([self.dir_with_files_path], recursive=True, force=True)
        result = trash_result[0]
        self.assertIn('cfg_path', result)

    def test_existence_of_config_file(self):
        trash_result = self.trash.remove([self.dir_with_files_path], recursive=True, force=True)
        cfg_path = trash_result[0]['cfg_path']
        self.assertTrue(os.path.exists(cfg_path))

    def test_exists_fields_of_config(self):
        trash_result = self.trash.remove([self.dir_with_files_path], recursive=True, force=True)
        cfg_path = trash_result[0]['cfg_path']

        config = Config(cfg_path)
        config.read()

        for field in ['name', 'origin_path', 'trash_name', 'trash_path', 'hashname', 'size', 'last_modified']:
            self.assertIn(field, config)

    def test_correctness_of_config(self):
        trash_result = self.trash.remove([self.dir_with_files_path], recursive=True, force=True)
        cfg_path = trash_result[0]['cfg_path']

        config = Config(cfg_path)
        config.read()

        self.assertEqual(os.path.basename(self.dir_with_files_path), config['name'])
        self.assertTrue(self.dir_with_files_path, config['origin_path'])

        trash_name = '{}_{}'.format(config['name'], config['hashname'])
        self.assertEqual(trash_name, config['trash_name'])

        trash_path = os.path.join(self.trash.location_path, config['trash_name'])
        self.assertEqual(trash_path, config['trash_path'])
    #
    # def test_trash_repair(self):
    #     file_protected_path = os.path.join(self.dir_with_files_path, 'file_0')
    #     os.chmod(file_protected_path, 0444)
    #     with self.assertRaises(exception.WriteProtectedFileOrDirectiry):
    #         self.trash.remove([self.dir_with_files_path], recursive=True)
    #
    #     self.assertTrue(len(os.listdir(self.trash.location_path)), 2)
    #     self.assertTrue(len(filter(lambda path: path.endswith('.cfg'), os.listdir(self.trash.location_path))), 1)
    #
    def test_dry_run(self):
        before_remove_len = len(self._get_paths(self.dir_with_files_path))

        trash_result = self.trash.remove([self.dir_with_files_path], recursive=True, force=True, dry_run=True)
        cfg_path = trash_result[0]['cfg_path']
        self.assertIsNone(cfg_path)
        self.assertFalse(self._check_file_exist_in_trash(self.dir_with_files_path))

        after_dry_run_remove_len = len(self._get_paths(self.dir_with_files_path))
        self.assertEqual(before_remove_len, after_dry_run_remove_len)
        self.assertFalse(self._check_file_exist_in_trash(self.dir_with_files_path))

    def test_restore(self):
        paths = self._get_paths_with_none_fail_reason(self.dir_with_files_path)

        trash_result = self.trash.remove([self.dir_with_files_path], recursive=True, force=True)
        cfg_path = trash_result[0]['cfg_path']

        config = Config(cfg_path)
        config.read()

        trash_path = config['trash_path']

        self.assertFalse(os.path.exists(self.dir_with_files_path))
        self.assertTrue(os.path.exists(trash_path))

        self.trash.restore([trash_path], recursive=True, force=True)

        self.assertFalse(os.path.exists(trash_path))
        self.assertFalse(os.path.exists(cfg_path))


        for path in paths:
            self.assertTrue(os.path.exists(path))

    # def test_erase(self):
    #     self.trash.remove([self.dir_with_files_path], recursive=True, force=True)
    #     self.assertTrue(os.listdir(self.POLYGON_DIR))
    #
    #     self.trash.erase()
    #     self.assertFalse(os.path.exists(self.trash.location_path))

if __name__ == '__main__':
    unittest.main()