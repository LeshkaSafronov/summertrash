import os
import logging
from default_config import SETTINGS

def get_logger(name=SETTINGS['name'],
               log_path=SETTINGS['log_path'],
               log_level=SETTINGS['log_level'],
               silent=False,
               **kwargs):
    log_path = os.path.expanduser(log_path)

    logger = logging.getLogger(name)
    logger.setLevel(logging._levelNames[log_level])

    formatter = logging.Formatter('%(name)s - %(message)s')

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)

    file_handler = logging.FileHandler(log_path)
    file_handler.setFormatter(formatter)

    if not silent:
        logger.addHandler(stream_handler)
    logger.addHandler(file_handler)

    return logger
