""" Tis module consist operation for work with trash."""

import os
import re
import uuid
import base64
import hashlib
import shutil
import exception
import logging

from config import Config, NotConfigException
from prettytable import PrettyTable
from datetime import datetime
from default_config import SETTINGS
from log import get_logger
from multiprocessing.pool import ThreadPool
from threading import Lock


class Trash(object):
    """ This class is intended for for work with trash."""

    TIME_FORMAT = '%Y-%m-%d %H:%M:%S'
    STATUS = {
        None: 'Success',
        'not_existed': 'Not Existed',
        'is_directory': 'Is Directory',
        'not_empty': 'Not Empty',
        'write_protected': 'Write Protected',
        'permission_denied': 'Permission Denied',
    }

    def __init__(self,
                 name=SETTINGS['name'],
                 location_path=SETTINGS['location_path'],
                 log_path=SETTINGS['log_path'],
                 log_level=SETTINGS['log_level'],
                 restore_policy=SETTINGS['restore_policy'],
                 autoclean_policy=SETTINGS['autoclean_policy'],
                 keep_alive_time=SETTINGS['keep_alive_time'],
                 keep_alive_size=SETTINGS['keep_alive_size'],
                 requester=None,
                 config=None,
                 silent=False,
                 **kwargs):
        self.lock = Lock()
        self.name = name
        self.location_path = location_path
        self.log_path = log_path
        self.restore_policy = restore_policy
        self.autoclean_policy = autoclean_policy
        self.keep_alive_time = keep_alive_time
        self.keep_alive_size = keep_alive_size
        self.requester = requester

        self.logger = get_logger(name=self.name,
                                 log_path=log_path,
                                 silent=silent,
                                 log_level=log_level)

        if config:
            if not isinstance(config, Config):
                raise NotConfigException()
            self._initialize_by_config(config)

        self.location_path = os.path.expanduser(location_path)

        self.create_trash_folder()
        self.repair()

    def _initialize_by_config(self, config):
        config.read()
        data = config.get_all()
        for key, value in data.items():
            if self.__getattribute__(key) != value:
                self.__setattr__(key, value)

    def _mkdir(self, path, **kwargs):
        dry_run = kwargs.get('dry_run', False)
        verbose = kwargs.get('verbose', False)
        if not dry_run:
            with self.lock:
                os.mkdir(path)

        if verbose:
            self.logger.debug('created directory \'{}\''.format(path))

    def _rename(self, src, dst, **kwargs):
        dry_run = kwargs.get('dry_run', False)
        verbose = kwargs.get('verbose', False)
        if not dry_run:
            with self.lock:
                shutil.move(src, dst)

        if verbose:
            if os.path.isdir(src):
                self.logger.debug('renamed directory \'{}\' --> \'{}\''.format(src, dst))
            else:
                self.logger.debug('renamed file \'{}\' --> \'{}\''.format(src, dst))

    def _rmdir(self, path, **kwargs):
        dry_run = kwargs.get('dry_run', False)
        verbose = kwargs.get('verbose', False)

        if not dry_run:
            os.rmdir(path)

        if verbose:
            self.logger.debug('removed directory \'{}\''.format(path))

    def _remove(self, path, **kwargs):
        dry_run = kwargs.get('dry_run', False)
        verbose = kwargs.get('verbose', False)
        if not dry_run:
            with self.lock:
                os.remove(path)

        if verbose:
            self.logger.debug('removed  \'{}\''.format(path))

    def create_trash_folder(self):
        """Create trash folders.q"""
        try:
            self._mkdir(self.location_path)
        except OSError as e:
            self.logger.warning('Cannot create trash folder. fail_reason: {}'.format(str(e)))

    def _handle_error(self, path, e, fail_reason, errors='strict', **kwargs):
        if errors == 'strict':
            self.repair()
            raise e(path)
        elif errors == 'ignore':
            return fail_reason

    def _check_top_level_path(self, path, **kwargs):
        recursive, force, dir, errors = kwargs.get('recursive'), kwargs.get('force'), \
                                        kwargs.get('dir'), kwargs.get('errors', 'strict')
        if not os.path.exists(path):
            if not force:
                return self._handle_error(path, exception.NotExistedFileOrDirectory, 'not_existed', **kwargs)
            else:
                return 'not_existed'
        else:
            if os.path.isdir(path):
                if not recursive:
                    if not dir:
                        return self._handle_error(path, exception.IsDirectoryException, 'is_directory', **kwargs)
                    elif os.listdir(path):
                        return self._handle_error(path, exception.NotEmptyDirectoryException, 'not_empty', **kwargs)
            return None

    def get_hashname(self, path):
        while True:
            filesize = os.path.getsize(path)
            uuid_hash = uuid.uuid4()

            base64_hash = base64.b64encode("{}:{}:{}".format(path, filesize, uuid_hash))
            md5_hash = hashlib.md5(base64_hash).hexdigest()
            if self.check_hashname(md5_hash):
                return md5_hash

    def check_hashname(self, hashname):
        hashnames = map(lambda path: self.get_hashname_from_path(path), os.listdir(self.location_path))
        return hashname not in hashnames

    def autoclean(self):
        files_info = self.get_files()
        for info in files_info:
            if self.autoclean_policy == 'last_modified' and self.keep_alive_time != 0:
                delta = (datetime.now() - datetime.strptime(info['last_modified'], "%Y-%m-%d %H:%M:%S")).total_seconds()
                if delta > self.keep_alive_time:
                    self.erase([info['trash_path']], recursive=True, force=True)
            elif self.autoclean_policy == 'size' and self.keep_alive_size != 0:
                if os.path.getsize(info['trash_path']) > self.keep_alive_size:
                    self.erase([info['trash_path']], recursive=True, force=True)

    def erase(self, paths, **kwargs):
        result = []
        for path in paths:
            output = self._erase(path, **kwargs)
            result.append({'result': output})
        return result

    def save_config(self, path, trash_path, hashname):
        trash_name = os.path.basename(trash_path)

        cfg_path = os.path.join(self.location_path, '{}.cfg'.format(trash_name))
        config = Config(cfg_path)

        config['name'] = os.path.basename(path)
        config['origin_path'] = path

        config['trash_name'] = os.path.basename(trash_path)
        config['trash_path'] = trash_path

        config['size'] = self.get_size(trash_path)
        config['hashname'] = hashname
        config['last_modified'] = datetime.now().strftime(self.TIME_FORMAT)

        config['is_dir'] = os.path.isdir(trash_path)

        config.save()

        return cfg_path

    def _process_write_protected_path(self, path, **kwargs):
        interactive = kwargs.get('interactive', False)
        force = kwargs.get('force', False)
        if not force:
            if interactive and self.requester:
                output = self.requester.write_protected(path)
                if output.lower() == 'y':
                    return None
                else:
                    return 'pass'
            else:
                return self._handle_error(path, exception.WriteProtectedFileOrDirectiry, 'write_protected', **kwargs)
        else:
            return None

    def _check_replace_policy(self, src, dst, **params):
        replace_policy = params.get('replace_policy', self.restore_policy)
        if not replace_policy:
            return True
        else:
            if replace_policy == 'name':
                return os.path.basename(src) < os.path.basename(dst)
            elif replace_policy == 'size':
                return self.get_size(src) < self.get_size(dst)
            elif replace_policy == 'last_modified':
                return os.path.getmtime(src) < os.path.getmtime(dst)

    def prepare_dst_path(self, dst, path, initial_path, hashname, concat=True):
        relative_path = '{}'.format(path[len(initial_path)+1:])

        parts = relative_path.split('/')
        root_relative_path, remainder_relative_path = parts[0], '/'.join(parts[1:])

        if concat:
            root_relative_path = '{}_{}'.format(root_relative_path, hashname)
        else:
            root_relative_path = root_relative_path.split('_{}'.format(hashname))[0]

        if remainder_relative_path:
            dst_path = os.path.join(dst, root_relative_path, remainder_relative_path)
        else:
            if root_relative_path:
                dst_path = os.path.join(dst, root_relative_path)
            else:
                dst_path = dst
        return dst_path

    @classmethod
    def get_file_last_modified(cls, path):
        return datetime.fromtimestamp(os.path.getmtime(path)).strftime(cls.TIME_FORMAT)

    @classmethod
    def get_file_info(cls, path):
        return {'name': os.path.basename(path),
                'path': path,
                'size': os.path.getsize(path),
                'last_modified': cls.get_file_last_modified(path)}

    @classmethod
    def info_to_str(cls, info):
        pretty_table = PrettyTable()
        pretty_table._set_field_names(info.keys())
        pretty_table.add_row(info.values())
        return pretty_table.get_string()

    def replace(self, src, dst, **params):
        workers = params.get('workers', 1)
        hashname = params.get('hashname', '')

        result = {}
        concate_hashname = True if dst == self.location_path else False

        paths = []
        try:
            paths = list(reversed(self.get_paths(src)))
        except OSError as e:
            if e.errno == 13:
                fail_reason = self._handle_error(src, exception.PermissionDenied, 'permission_denied', **params)
                return {src: fail_reason}

        excluded_dirs = []
        dirs = [path for path in paths if os.path.isdir(path)]
        files = [path for path in paths if not os.path.isdir(path)]

        # first, we need to create folders in trash
        for path in dirs:
            dst_path = self.prepare_dst_path(dst, path, src, hashname, concat=concate_hashname)
            create_dir_result = self.create_dir(path, dst_path, excluded_dirs, **params)
            result.update(create_dir_result)

        # then, replace files
        arguments = []
        for path in files:
            dst_path = self.prepare_dst_path(dst, path, src, hashname, concat=concate_hashname)
            arguments.append(([path, dst_path, excluded_dirs], params))

        pool = ThreadPool(workers)
        pool_output = pool.map(lambda (args, kwargs): self.replace_file(*args, **kwargs), arguments)
        pool.close()

        for output in pool_output:
            result.update(output)

        # clean source directory from unnecessary files
        for path in reversed(dirs):
            try:
                if path not in excluded_dirs:
                    self._rmdir(path, **params)
            except OSError:
                pass

        return result

    def create_dir(self, path, dst_path, excluded_dirs, callback=None, **params):
        interactive = params.get('interactive', False)

        result = {}
        if not filter(lambda excluded_path: path.startswith(excluded_path), excluded_dirs):
            try:
                if not os.access(path, os.W_OK):
                    fail_reason = self._process_write_protected_path(path, **params)
                    if fail_reason:
                        excluded_dirs.append(path)
                        return {path: fail_reason}

                if os.path.exists(dst_path):
                    if interactive and self.requester:
                        if not self.requester.merge(path, dst_path):
                            excluded_dirs.append(path)
                            return {path: 'pass'}
                    else:
                        if not self._check_replace_policy(path, dst_path, **params):
                            excluded_dirs.append(path)
                            return {path: 'pass'}

                if not os.path.exists(dst_path):
                    if interactive and self.requester:
                        if not self.requester.interactive_replace_dir(path, dst_path):
                            excluded_dirs.append(path)
                            return {path: 'pass'}

                self._mkdir(dst_path, **params)
                if callback:
                    callback(os.path.getsize(dst_path))
                result[path] = None
            except OSError as e:
                if e.errno == 13:
                    fail_reason = self._handle_error(path, exception.PermissionDenied, 'permission_denied', **params)
                    result[path] = fail_reason
        else:
            parent_dir = os.path.dirname(path)
            result[path] = result[parent_dir]
        return result

    def replace_file(self, path, dst_path, excluded_dirs, callback=None, **params):
        interactive = params.get('interactive', False)
        result = {}

        if not filter(lambda excluded_path: path.startswith(excluded_path), excluded_dirs):
            try:
                if not os.access(path, os.W_OK):
                    fail_reason = self._process_write_protected_path(path, **params)
                    if fail_reason:
                        return {path: fail_reason}

                if os.path.exists(dst_path):
                    if interactive and self.requester:
                        if not self.requester.merge(path, dst_path):
                            return {path: 'pass'}
                    else:
                        if not self._check_replace_policy(path, dst_path, **params):
                            return {path: 'pass'}

                else:
                    if interactive and self.requester:
                        if not self.requester.interactive_replace_file(path, dst_path):
                            return {path: 'pass'}

                self._rename(path, dst_path, **params)
                if callback:
                    callback(os.path.getsize(dst_path))
                result[path] = None
            except OSError as e:
                if e.errno == 13:
                    fail_reason = self._handle_error(path, exception.PermissionDenied, 'permission_denied', **params)
                    result[path] = fail_reason
        else:
            parent_path = os.path.dirname(path)
            result[path] = result[parent_path]
        return result

    def _erase(self, src, **params):
        result = {}
        paths = list(self.get_paths(src))
        dirs = [path for path in paths if os.path.isdir(path)]
        files = [path for path in paths if not os.path.isdir(path)]

        arguments = []
        for path in files:
            arguments.append(([path], params))

        pool = ThreadPool(64)
        pool_output = pool.map(lambda (args, kwargs): self.remove_file(*args, **kwargs), arguments)
        pool.close()

        for output in pool_output:
            result.update(output)

        for path in dirs:
            output = self.remove_dir(path, **params)
            result.update(output)

        self._remove('{}.cfg'.format(src), **params)
        return result

    def remove_file(self, src, callback=None, **params):
        if callback:
            callback(os.path.getsize(src))
        self._remove(src, **params)
        return {src: None}

    def remove_dir(self, src, callback=None, **params):
        if callback:
            callback(os.path.getsize(src))
        self._rmdir(src, **params)
        return {src: None}

    def clean(self, **kwargs):
        paths = [os.path.join(self.location_path, path)
                 for path in os.listdir(self.location_path) if not path.endswith('.cfg')]
        self.erase(paths, **kwargs)
        self._rmdir(self.location_path, **kwargs)

    def remove(self, paths, regex=None, **params):
        params['restore_policy'] = None

        if regex:
            paths = [os.path.abspath(path) for path in paths if re.match(regex, os.path.basename(path))]
        else:
            paths = [os.path.abspath(path) for path in paths]

        result = []
        for path in paths:
            reason = self._check_top_level_path(path, **params)
            if reason:
                result.append({'result': {path: reason}, 'cfg_path': None})
                continue

            parent_path = os.path.dirname(path)
            hashname = self.get_hashname(path)
            dst_path = self.prepare_dst_path(self.location_path, path, parent_path, hashname)

            output = self.replace(path, dst_path, hashname=hashname, **params)

            cfg_path = None
            if os.path.exists(dst_path):
                cfg_path = self.save_config(path, dst_path, hashname)

            result.append({'result': output, 'cfg_path': cfg_path})
        return result

    def repair_origin_path(self, path, **params):
        parts = path.split('/')[1:-1]
        current_path = '/'
        for part in parts:
            current_path += '{}/'.format(part)
            if not os.path.exists(current_path):
                self._mkdir(current_path, **params)

    def restore(self, paths, **params):
        result = []

        for path in paths:
            reason = self._check_top_level_path(path, **params)
            if reason:
                result.append({'result': {path: reason}, 'cfg_path': None})
                continue

            config_path = '{}.cfg'.format(path)
            config = Config(config_path)
            config.read()

            path = config['trash_path']

            dst_path = config['origin_path']
            self.repair_origin_path(dst_path, **params)

            hashname = config['hashname']

            output = self.replace(path, dst_path, hashname=hashname, **params)

            if os.path.exists(path):
                self.save_config(dst_path, path, hashname)
            else:
                self._remove(config_path, **params)
            result.append({'result': output})
        return result

    def repair(self):
        cfg_paths = filter(lambda paths: paths.endswith('.cfg'), os.listdir(self.location_path))
        deleted_names = list(map(lambda paths: paths.split('.cfg')[0], cfg_paths))
        crashed_files = filter(lambda path: path not in deleted_names and \
                                            path not in cfg_paths, os.listdir(self.location_path))
        for filepath in crashed_files:
            fullpath = os.path.join(self.location_path, filepath)
            hashname = self.get_hashname_from_path(filepath)
            self.save_config(fullpath, fullpath, hashname)

    def get_hashname_from_path(self, path):
        return path.split('_')[-1]

    def view(self, *args):
        cfg_paths = filter(lambda path: path.split('.')[-1] == 'cfg', os.listdir(self.location_path))
        cfg_abs_paths = map(lambda path: os.path.join(self.location_path, path), cfg_paths)
        data = []
        for path in cfg_abs_paths:
            config = Config(path)
            config.read()
            data.append(config.get_all())

        pretty_table = PrettyTable()

        args = args if args else ['name', 'origin_path', 'trash_name', 'trash_path', 'hashname', 'size', 'last_modified', 'is_dir']
        for column in args:
            pretty_table.add_column(column, map(lambda config: config[column], data))

        return pretty_table.get_string()

    def get_file_info_from_config(self, path):
        config = Config(path)
        config.read()
        return config.get_all()

    def get_files(self, **kwargs):
        cfg_paths = filter(lambda path: path.split('.')[-1] == 'cfg', os.listdir(self.location_path))
        cfg_abs_paths = map(lambda path: os.path.join(self.location_path, path), cfg_paths)

        pool = ThreadPool(64)
        pool_output = pool.map(self.get_file_info_from_config, cfg_abs_paths)
        pool.close()

        return pool_output

    def get_size(self, src):
        reason = self._check_top_level_path(src, recursive=True, force=True, errors='ignore')
        if not reason:
            paths = self.get_paths(src)
            return sum(os.path.getsize(path) for path in paths if os.path.exists(path))
        else:
            return 0

    def get_paths(self, path):
        filepaths = []
        if os.path.isdir(path) and os.listdir(path):
            for inner_path in os.listdir(path):
                filepaths += self.get_paths(os.path.join(path, inner_path))
            filepaths += [path]
        else:
            filepaths = [path]
        return filepaths

    def update_settings(self, **params):
        self.name = params.get('name', self.name)
        self.restore_policy = params.get('restore_policy', self.restore_policy)
        self.autoclean_policy = params.get('autoclean_policy', self.autoclean_policy)
        self.keep_alive_time = params.get('keep_alive_time', self.keep_alive_time)
        self.keep_alive_size = params.get('keep_alive_size', self.keep_alive_size)

