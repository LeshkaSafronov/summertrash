import json as ejson
import ConfigParser
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('config')


class NotConfigException(Exception):
    def __str__(self):
        return 'Instance is not Config'


class CannotReadConfigException(Exception):
    def __init__(self, filename):
        self._filename = filename

    def __str__(self):
        return 'Cannot read config'


class Config(object):

    def __init__(self, path, json=True):
        self._section = 'Trash'
        self._json = json

        self._config_json = {}

        self._config = ConfigParser.ConfigParser()
        self._config.add_section(self._section)

        self._path = path

    def read_json(self, path):
        with open(path, 'rb') as f:
            self._config_json = ejson.load(f)
        self._json = True

    def read_config(self, path):
        self._config.read(path)
        self._json = False

    def read(self):
        self._success_open = False
        try:
            self.read_json(self._path)
            self._success_open = True
        except Exception as e:
            pass
            # logging.warning('Config read error: {}'.format(str(e)))

        try:
            self.read_config(self._path)
            self._success_open = True
        except Exception as e:
            pass
            # logging.warning('Config read error: {}'.format(str(e)))

        if not self._success_open:
            raise CannotReadConfigException(self._path)

    def __setitem__(self, key, value):
        if self._json:
            self._config_json[key] = value
        else:
            self._config.set(self._section, key, value)

    def __getitem__(self, key):
        if self._json:
            return self._config_json[key]
        else:
            return self._config.get(self._section, key)

    def __contains__(self, key):
        if self._json:
            return key in self._config_json
        else:
            d = dict(self._config.items(self._section))
            return key in d

    def save(self):
        with open(self._path, 'wb') as f:
            if self._json:
                ejson.dump(self._config_json, f)
            else:
                self._config.write(f)

    def get_all(self):
        if self._json:
            return self._config_json
        else:
            return dict(self._config.items(self._section))
