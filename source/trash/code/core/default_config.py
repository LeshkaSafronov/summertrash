SETTINGS = {
    "name": 'Trash',
    "location_path": "~/.trash",
    "log_path": "~/.trash.log",
    'log_level': "DEBUG",
    "free_space": 1048576,
    "restore_policy": None,
    "autoclean_policy": None,
    "keep_alive_time": 0,
    "keep_alive_size": 0
}