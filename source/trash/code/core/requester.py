import os
from trash import Trash

class Requester:
    def merge(self, trash_path, source_path):
        if os.path.isdir(trash_path):
            print('Merge folder "{}"'.format(os.path.basename(source_path)))
        else:
            print('Replace file "{}"'.format(os.path.basename(source_path)))

        print 'A newer folder with the same name already exists in "{}".'.format(os.path.dirname(source_path))

        if not os.path.isdir(trash_path):
            print('Replacing it will overwrite its content')

        source_file_info = Trash.get_file_info(source_path)
        trash_file_info = Trash.get_file_info(trash_path)
        print '\nOrigin file:'
        print Trash.info_to_str(source_file_info), '\n'

        print('Replace with:')
        print Trash.info_to_str(trash_file_info), '\n'

        if os.path.isdir(trash_path):
            return raw_input('Do you want to merge this folder? [y/N]').lower() == 'y'
        else:
            return raw_input('Do you want to replace this file? [y/N]').lower() == 'y'

    def write_protected(self, path):
        if os.path.isdir(path):
            return raw_input('Replace write-protected directory \'{}\''.format(path))
        else:
            if os.path.getsize(path) > 0:
                return raw_input('Replace write-protected regular file \'{}\''.format(path))
            else:
                return raw_input('Replace write-protected regular empty file \'{}\''.format(path))

    def interactive_replace_file(self, path, dst_path, **kwargs):
        if os.path.getsize(path) > 0:
            return raw_input('Replace regular file \'{}\' to \'{}\' ? [y/N]'.format(path, dst_path)).lower() == 'y'
        else:
            return raw_input('Replace regular empty file \'{}\' to \'{}\' ? [y/N]'.format(path, dst_path)).lower() == 'y'

    def interactive_replace_dir(self, path, dst_path, **kwargs):
        return raw_input('Replace directory \'{}\' to \'{}\' ? [y/N]'.format(path, dst_path)).lower() == 'y'
