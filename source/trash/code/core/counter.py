from threading import Lock


class Counter(object):
    def __init__(self, trash, job, paths):
        self._lock = Lock()

        self.trash = trash
        self.job = job
        self.job.total_bytes = sum(trash.get_size(path) for path in paths)
        self.job.save()

    def __call__(self, bytes):
        with self._lock:
            self.job.bytes_processed += bytes
            self.job.save()
