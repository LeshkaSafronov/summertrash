import argparse
import os
import core.exit_codes as exit_codes
import core.exception as trash_exception
import logging

from core.trash import Trash
from core.requester import Requester
from core.default_config import SETTINGS
from core.config import Config



def parse_arguments():
    parser = argparse.ArgumentParser(description='Parser for trash')
    parser.add_argument('paths', nargs='*')

    parser.add_argument('-f', '--force', action='store_true')
    parser.add_argument('-d', '--dir', action='store_true')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-r', '-R', '--recursive', action='store_true')
    parser.add_argument('-t', '--tables', nargs='*', default=[])
    parser.add_argument('-e', '--erase', action='store_true')
    parser.add_argument('-p', '--parallel', action='store_true')
    parser.add_argument('-i', '--interactive', action='store_true')
    parser.add_argument('-c', '--clean', action='store_true')
    parser.add_argument('--restore', action='store_true')
    parser.add_argument('--view', action='store_true')
    parser.add_argument('--dry-run', action='store_true')
    parser.add_argument('--workers', default=5)

    parser.add_argument('--name', default=SETTINGS['name'])
    parser.add_argument('--location_path', default=SETTINGS['location_path'])
    parser.add_argument('--log-path', default=SETTINGS['log_path'])
    parser.add_argument('--log-level', default=SETTINGS['log_level'])
    parser.add_argument('--free-space', default=SETTINGS['free_space'])
    parser.add_argument('--replace-policy', default=SETTINGS['restore_policy'])
    parser.add_argument('--autoclean-policy', default=SETTINGS['autoclean_policy'])
    parser.add_argument('--keep-alive-time', default=SETTINGS['keep_alive_time'])
    parser.add_argument('--keep-alive-size', default=SETTINGS['keep_alive_size'])
    parser.add_argument('--silent', action='store_true')
    parser.add_argument('--regex', default=None)
    parser.add_argument('--cfg-path', default=None)

    return parser.parse_args()


def main():
    data = parse_arguments()

    params = dict(data._get_kwargs())

    if params.get('dry_run'):
        params['verbose'] = True

    silent = params.get('silent')

    cfg_path = params.pop('cfg_path', None)
    if cfg_path:
        abs_cfg_path = os.path.abspath(cfg_path)

        config = Config(abs_cfg_path)
        config.read()

        trash = Trash(config=config, requester=Requester(), **params)
    else:
        trash = Trash(requester=Requester(), **params)

    trash.autoclean()

    try:
        if data.view:
            print(trash.view(*params['tables']))
        elif data.restore:
            params.update({'recursive': True})
            trash.restore(**params)
        elif data.erase:
            params.update({'recursive': True,
                           'force': True})
            trash.erase(**params)
        elif data.clean:
            params.update({'recursive': True,
                           'force': True})
            params.pop('paths')
            trash.clean(**params)
        else:
            trash.remove(**params)
    except trash_exception.IsDirectoryException as e:
        if silent:
            os._exit(exit_codes.EXIT_CODE_IS_DIRECTORY)
        else:
            logging.exception('summertrash: {}'.format(str(e)))

    except trash_exception.NotExistedFileOrDirectory as e:
        if silent:
            os._exit(exit_codes.EXIT_CODE_NOT_EXISTED_FILE_OR_DIRECRORY)
        else:
            logging.exception('summertrash: {}'.format(str(e)))

    except trash_exception.NotEmptyDirectoryException as e:
        if silent:
            os._exit(exit_codes.EXIT_CODE_NOT_EMPTY_DIRECTORY)
        else:
            logging.exception('summertrash: {}'.format(str(e)))

    except trash_exception.WriteProtectedFileOrDirectiry as e:
        if silent:
            os._exit(exit_codes.EXIT_CODE_WRITE_PROTECTED_FILE_OR_DIRECTORY)
        else:
            logging.exception('summertrash: {}'.format(str(e)))

    except trash_exception.PermissionDenied as e:
        if silent:
            os._exit(exit_codes.EXIT_CODE_PERMISSION_DENIED)
        else:
            logging.exception('summertrash: {}'.format(str(e)))
    except Exception as e:
        if silent:
            os._exit(exit_codes.EXIT_CODE_OTHER_ERROR)
        else:
            logging.exception('summertrash: {}'.format(str(e)))
