class TrashException(Exception):
    def __init__(self, filename):
        self._filename = filename

    def __str__(self):
        return "{}: {}".format(self._filename, self.reason)


class IsDirectoryException(TrashException):
    reason = 'Is a directory'


class NotEmptyDirectoryException(TrashException):
    reason = 'Directory not empty'


class NotExistedFileOrDirectory(TrashException):
    reason = 'No such file or directory'


class WriteProtectedFileOrDirectiry(TrashException):
    reason = 'Write-protected file or directory'


class PermissionDenied(TrashException):
    reason = 'Permission denied'
