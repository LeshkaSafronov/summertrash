#!/usr/bin/env bash
set -e

cd ${CODE_DIR}
python wait_postgres.py trashes_trash && \
python server.py