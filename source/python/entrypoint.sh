#!/usr/bin/env bash
set -e

cd ${CODE_DIR}
python wait_postgres.py pg_database && \
python manage.py migrate && \
python manage.py collectstatic --no-input && \
python manage.py runserver 0.0.0.0:8000