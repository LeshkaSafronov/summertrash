$(".open-delete-modal").click(function(){
    $("#delete-trash-modal").modal('show');
});

$(".trash-delete-modal-select").click(function() {
    trash_id = $(this).attr('value');
    $(".trash-delete-modal-selected").text($(this).text());
    $(".selected-trash-id").attr('value', trash_id);

    $(".delete-host-files").attr('type', 'submit');
    $(".delete-host-files").removeClass('disabled');
})

$(".delete-host-files").click(function() {
    $(".host-files-form").attr('action', '/host/delete');
})

$(".trash-restore-button").click(function() {
    trash_id = $("#trash-id").attr('value');
    $(".trash-files-form").attr('action', '/trashes/' + trash_id + '/restore');
})


$(".trash-delete-button").click(function() {
    trash_id = $("#trash-id").attr('value');
    $(".trash-files-form").attr('action', '/trashes/' + trash_id + '/delete');
})

$(".choose-restore-policy").click(function() {
    $('#trash-restore-policy').text($(this).text());
    $('#trash-restore-policy-input').attr('value', $(this).attr('value'))
})

$(".choose-autoclean-policy").click(function() {
    $('#trash-autoclean-policy').text($(this).text());
    $('#trash-autoclean-policy-input').attr('value', $(this).attr('value'))
})

$("#select-all-delete-label").click(function() {
    if ($("#select-all-delete").prop( "checked" ) == true) {
        $(".host-files-checkbox").prop("checked", false);
    }
    else {
        $(".host-files-checkbox").prop("checked", true);
    }
    console.log($("#select-all-delete").prop( "checked" ))
})


$("#select-all-trash-files-label").click(function() {
    if ($("#select-all-trash-files").prop( "checked" ) == true) {
        $(".trash-files-checkbox").prop("checked", false);
    }
    else {
        $(".trash-files-checkbox").prop("checked", true);
    }
    console.log($("#select-all-trash-files").prop( "checked" ));
})