from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^files(.*)$', views.file_paths, name='files'),
    url(r'^delete$', views.delete_files, name='delete_files'),
]