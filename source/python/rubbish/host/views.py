# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from datetime import datetime

from django.shortcuts import render, redirect
from trashes.models import Trash
from services import HostService
from .forms import DeleteForm
from django.http import HttpResponse


TEMPLATE_PATH = 'host.html'
TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

service = HostService()


def get_context_data(filepath):
    context = {'trashes': Trash.objects.all()}

    context['files'] = []

    st = os.statvfs('/host')
    context['free'] = round(st.f_bavail * st.f_frsize / float(1024 ** 3), 2)
    context['used'] = round((st.f_blocks - st.f_bfree) * st.f_frsize / float(1024 ** 3), 2)
    context['total'] = round(st.f_blocks * st.f_frsize / float(1024 ** 3), 2)
    context['available_space_percent'] = int(round(context['used'] / context['total'] * 100))

    context['partition_paths'] = [{'name': 'host', 'link': ''}]

    path_parts = filepath.split('/')[2:]
    for i, part in enumerate(path_parts):
        data = {
            'name': part,
            'link': '/{}'.format('/'.join(path_parts[:i+1]))
        }
        context['partition_paths'].append(data)

    try:
        paths = [os.path.join(filepath, path) for path in os.listdir(filepath)]
        for path in paths:
            data = {'name': os.path.basename(path),
                    'is_dir': os.path.isdir(path),
                    'last_modified': datetime.fromtimestamp(os.path.getmtime(path)).strftime(TIME_FORMAT),
                    'link': path[5:],
                    'path': path,
                    'size': '-'}
            if not os.path.isdir(path):
                data['size'] = os.path.getsize(path)
            context['files'].append(data)
    except OSError:
        pass

    return context


def file_paths(request, *args, **kwargs):
    url = request.get_full_path()
    context = {'current_link': url}

    if url == '/host/files':
        context.update(get_context_data('/host'))
    else:
        request_filepath = url.split('/host/files/')[1]
        filepath = os.path.join('/host', request_filepath)
        context.update(get_context_data(filepath))

    return render(request, TEMPLATE_PATH, context)


def delete_files(request, *args, **kwargs):
    if request.method == 'POST':
        form = DeleteForm(request.POST)
        paths = request.POST.getlist('paths')
        current_link = request.POST.get('current_link')
        if form.is_valid():
            data = form.cleaned_data
            data.update({'paths': paths,
                         'errors': 'ignore'})
            trash_id = data.pop('trash_id')
            service.delete_files(trash_id, data)
        return redirect(current_link)
    else:
        return HttpResponse(404)