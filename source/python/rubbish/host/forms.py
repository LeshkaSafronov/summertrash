from django import forms
from trashes.models import Trash

class DeleteForm(forms.Form):
    trash_id = forms.IntegerField()
    recursive = forms.BooleanField(required=False)
    force = forms.BooleanField(required=False)
    dry_run = forms.BooleanField(required=False)
    verbose = forms.BooleanField(required=False)
    regex = forms.CharField(required=False)

    def clean_trash(self):
        trash_id = self.cleaned_data['trash_id']
        if not Trash.objects.filter(id=trash_id).exists():
            raise forms.ValidationError('Trash with id {} does not exists'.format(trash_id))
        return trash_id