import requests
import json

class Service(object):

    def __init__(self):
        self.endpoint = 'http://trash:7000/trashes'


class TrashService(Service):

    def init_trash(self, trash_id):
        requests.post('{}/{}/init'.format(self.endpoint, trash_id))

    def get_files(self, trash_id):
        resp = requests.get('{}/{}'.format(self.endpoint, trash_id))
        return resp.json()

    def restore_files(self, trash_id, paths):
        requests.post('{}/{}/restore'.format(self.endpoint, trash_id), data=json.dumps({'paths': paths}))

    def delete_files(self, trash_id, paths):
        requests.post('{}/{}/delete'.format(self.endpoint, trash_id), data=json.dumps({'paths': paths}))

    def clean_trash(self, trash_id):
        requests.post('{}/{}/clean'.format(self.endpoint, trash_id))

    def edit_trash(self, trash_id):
        requests.post('{}/{}/edit'.format(self.endpoint, trash_id))


class HostService(Service):

    def delete_files(self, trash_id, data):
        requests.post('{}/{}/add'.format(self.endpoint, trash_id), data=json.dumps(data))
