# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from django.db import models
from django.dispatch import receiver
from django.db.models import signals


class Trash(models.Model):
   RESTORE_POLICY_CHOICES = (
       (None, 'None'),
       ('name', 'By Name'),
       ('size', 'By Size'),
       ('last_modified', 'By Last Modified'),
   )

   AUTOCLEAN_POLICY_CHOICES = (
       (None, 'None'),
       ('size', 'By Size'),
       ('last_modified', 'By Last Modified'),
   )

   name = models.CharField(max_length=256, unique=True)
   location_path = models.CharField(max_length=256, unique=True, default="~/.trash")
   log_path = models.CharField(max_length=256, default='~/.trash.log')
   restore_policy = models.CharField(max_length=32, default=None, choices=RESTORE_POLICY_CHOICES, blank=True, null=True)
   autoclean_policy = models.CharField(max_length=32, default=None, choices=AUTOCLEAN_POLICY_CHOICES, blank=True, null=True)
   keep_alive_time = models.IntegerField(default=0)
   keep_alive_size = models.IntegerField(default=0)

   def __str__(self):
       return """id={},
                 name={},
                 location_path={},
                 log_path={},
                 free_space={},
                 restore_policy={},
                 autoclean_policy={},
                 keep_alive_time={},
                 keep_alive_size={}""".format(self.id,
                                              self.name,
                                              self.location_path,
                                              self.log_path,
                                              self.free_space,
                                              self.restore_policy,
                                              self.autoclean_policy,
                                              self.keep_alive_time,
                                              self.keep_alive_size)


class Job(models.Model):
   name = models.CharField(max_length=256)
   total_bytes = models.IntegerField(default=0)
   bytes_processed = models.IntegerField(default=0)
   trash = models.ForeignKey(Trash, related_name='jobs')
   done = models.BooleanField(default=False)
   result = models.FileField(null=True)


@receiver(signals.post_delete, sender=Job)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.result:
        os.remove(instance.result.path)
