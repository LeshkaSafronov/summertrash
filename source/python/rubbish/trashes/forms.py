from django import forms
from trashes.models import Trash


class TrashForm(forms.ModelForm):

    class Meta:
        model = Trash
        fields = ['name', 'restore_policy', 'autoclean_policy', 'keep_alive_time', 'keep_alive_size']

    def _check_name(self, name):
        if Trash.objects.filter(name=name).exists():
            raise forms.ValidationError("This Name is already exists")

    def clean_name(self):
        name = self.cleaned_data['name']
        if self.instance:
            if self.instance.name != name:
                self._check_name(name)
        else:
            self._check_name(name)
        return name

    def clean_keep_alive_time(self):
        keep_alive_time = self.cleaned_data['keep_alive_time']
        if keep_alive_time < 0:
            raise forms.ValidationError("Value must be > 0")
        return keep_alive_time

    def clean_keep_alive_size(self):
        keep_alive_size = self.cleaned_data['keep_alive_size']
        if keep_alive_size < 0:
            raise forms.ValidationError("Value must be > 0")
        return keep_alive_size

    def clean_restore_policy(self):
        restore_policy = self.cleaned_data['restore_policy']
        if restore_policy == 'None':
            return None
        return restore_policy

    def clean_autoclean_policy(self):
        autoclean_policy = self.cleaned_data['autoclean_policy']
        if autoclean_policy == 'None':
            return None
        return autoclean_policy
