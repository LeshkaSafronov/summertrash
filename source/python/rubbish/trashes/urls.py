from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^(?P<pk>[0-9]+)$', views.get, name='get'),
    url(r'^create$', views.create, name='create'),
    url(r'^jobs$', views.jobs, name='jobs'),
    url(r'^jobs/(?P<pk>[0-9]+)/delete$', views.delete_job, name='jobs-delete'),
    url(r'^(?P<pk>[0-9]+)/restore$', views.restore, name='restore'),
    url(r'^(?P<pk>[0-9]+)/delete$', views.delete, name='delete'),
    url(r'^(?P<pk>[0-9]+)/clean', views.clean, name='clean'),
    url(r'^(?P<pk>[0-9]+)/edit$', views.edit, name='edit'),
]