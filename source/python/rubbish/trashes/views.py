# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.shortcuts import render, redirect
from django.db import transaction
from django.forms.models import model_to_dict
from trashes.models import Trash, Job
from trashes.forms import TrashForm
from services import TrashService
from django.http import HttpResponse


TRASH_TEMPLATE = 'trash.html'
TRASH_CREATE_TEMPLATE = 'create.html'
TRASH_EDIT_TEMPLATE = 'edit.html'
JOB_TEMPLATE = 'job.html'

service = TrashService()


def get_context_data():
    context = {'trashes': Trash.objects.all()}
    return context


def get(request, pk, *args, **kwargs):
    try:
        trash = Trash.objects.get(id=pk)
    except Trash.DoesNotExist as e:
        return HttpResponse(404)

    context = get_context_data()
    context.update({'files': service.get_files(pk),
                    'trash': trash})

    return render(request, TRASH_TEMPLATE, context)


def create(request, *args, **kwargs):
    if request.method == 'POST':
        form = TrashForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            data['location_path'] = os.path.join('/polygon', data['name'])
            data['log_path'] = os.path.join('/log', data['name'])

            trash = Trash.objects.create(**data)
            service.init_trash(trash.id)

            return redirect('host:files', '')
        context = {'errors': form.errors}
        return render(request, TRASH_CREATE_TEMPLATE, context)
    else:
        return render(request, TRASH_CREATE_TEMPLATE)


def restore(request, pk, *args, **kwargs):
    try:
        trash = Trash.objects.get(id=pk)
    except Trash.DoesNotExists as e:
        return HttpResponse(404)

    if request.method == 'POST':
        paths = request.POST.getlist('paths')
        if paths:
            service.restore_files(trash.id, paths)
        return redirect('/trashes/{}'.format(pk))
    else:
        return HttpResponse(404)


def delete(request, pk, *args, **kwargs):
    try:
        trash = Trash.objects.get(id=pk)
    except Trash.DoesNotExists as e:
        return HttpResponse(404)

    if request.method == 'POST':
        paths = request.POST.getlist('paths')
        if paths:
            service.delete_files(pk, paths)
        return redirect('/trashes/{}'.format(pk))
    else:
        return HttpResponse(404)


def clean(request, pk, *args, **kwargs):
    try:
        trash = Trash.objects.get(id=pk)
    except Trash.DoesNotExists as e:
        return HttpResponse(404)

    if request.method == 'POST':
        trash = Trash.objects.get(id=pk)
        trash.delete()
        service.clean_trash(pk)

        return redirect('/host/files')
    else:
        return HttpResponse(404)


def edit(request, pk, *args, **kwargs):
    try:
        trash = Trash.objects.get(id=pk)
    except Trash.DoesNotExist as e:
        return HttpResponse(404)

    if request.method == 'GET':
        context = {'trash': trash,
                   'selected_restore_policy': trash.get_restore_policy_display(),
                   'selected_autoclean_policy': trash.get_autoclean_policy_display()}
        return render(request, TRASH_EDIT_TEMPLATE, context)
    else:
        form = TrashForm(request.POST, instance=trash)
        if form.is_valid():
            trash = form.save()
            service.edit_trash(trash.id)
            return redirect('host:files', '')
        else:
            context = {'trash': trash,
                       'selected_restore_policy': trash.get_restore_policy_display(),
                       'selected_autoclean_policy': trash.get_autoclean_policy_display(),
                       'errors': form.errors}
            return render(request, TRASH_EDIT_TEMPLATE, context)


def jobs(request, *args, **kwargs):
    context = get_context_data()
    context['jobs'] = []
    for job in Job.objects.all():
        data = model_to_dict(job)
        if data['done']:
            data['progress_percent'] = 100
        else:
            if data['total_bytes'] != 0:
                data['progress_percent'] = int(data['bytes_processed'] / data['total_bytes'] * 100)
            else:
                data['progress_percent'] = 100
        context['jobs'].append(data)
    return render(request, JOB_TEMPLATE, context)

def delete_job(request, pk, *args, **kwargs):
    try:
        job = Job.objects.get(id=pk)
    except Job.DoesNotExist:
        return HttpResponse(404)

    if request.method == 'POST':
        job.delete()
        return redirect('/trashes/jobs')
    else:
        return HttpResponse(404)
