.PHONY: down
down:
	docker-compose down

.PHONY: up
up: build-django-trash build-core-trash
	docker-compose up

.PHONY: restart
restart: down build-django-trash build-core-trash up

.PHONY: build-django-trash
build-django-trash: build-base-image
	docker build -t django-trash source/python

.PHONY: build-core-trash
build-core-trash: build-base-image
	docker build -t core-trash source/trash

.PHONY: build-base-image
build-base-image:
	docker build -t base-image source
